import { Component, OnInit } from '@angular/core';
import { ApiDodleBrokerService } from '../api-dodle-broker.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router} from '@angular/router';
import {Utilisateur} from "../../models/Utilisateur";
import {Invite} from "../../models/Invite";
import {Evenement} from "../../models/Evenement";
import {AppComponent} from "../app.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {forEachComment} from "tslint";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})

export class AccueilComponent implements OnInit {

  events: Evenement[];
  user: Utilisateur;
  option: string;
  constructor(public apiDodleBrokerService: ApiDodleBrokerService,
              private httpClient: HttpClient,
              private router: Router,
              private routeActive: ActivatedRoute,
              private snackBar: MatSnackBar,
              private authService: AuthService) { }

  ngOnInit(): void {
    this.apiDodleBrokerService.getUtilisateurParticipeByEvennement("5ece5572e274b914485e8795").subscribe((data) => {
      console.log("getUtilisateurParticipeByEvennement");
      console.log(data);
    })

    var nbvalue = new Array();
    this.user = JSON.parse(localStorage.getItem('currentUser'));

    this.option = this.routeActive.snapshot.paramMap.get("option");
    if (this.option == "myEvent") {
      this.apiDodleBrokerService.getEvenementNotClotureByIDCreateur(this.user._id).subscribe((events) => {
        this.events = events;
      });
    } else if (this.option == "myInvite") {
      this.apiDodleBrokerService.getEvenementByInviteIDUserEventNotCloture(this.user._id).subscribe((events) => {
        this.events = events
      })
    } else if (this.option == "myHistory") {
      this.apiDodleBrokerService.getEvenementClotureByIDCreateur(this.user._id).subscribe((events) => {
        this.events = events;
        this.apiDodleBrokerService.getEvenementByInviteIDUserEventCloture(this.user._id).subscribe((eventsInvite) => {
          this.events = this.events.concat(eventsInvite);
        })
      });
    }
    else {
      this.apiDodleBrokerService.getEvenementByIDCreateur(this.user._id).subscribe((events) => {
        this.events = events;
        this.apiDodleBrokerService.getEvenementByInviteIDUser(this.user._id).subscribe((eventsInvite) => {
          this.events = this.events.concat(eventsInvite);
          /*
          for(var i = 0; i < this.events.length; i++) {
            this.apiDodleBrokerService.getNbParticipeByEvent(this.events[i]._id).subscribe((nb) => {
              nbvalue.push(nb);
              this.nbParticipes = {nb :nbvalue,id : this.events[i]._id};
              console.log(nbvalue);
            });
          }
           */
        })
      });
    }



    this.showNotification();

  }

  // Récupère les invites et le notifier pour les afficher dans une popup
  async showNotification () {

     const notifier = await this.apiDodleBrokerService.getNotifierByUtilisateur(this.user._id).toPromise();

     console.log(notifier);
     let index = 0;
     let compteur = 0;
     const timeOut = 3000;

    for (var i = 0; i < notifier.length; i++) {
      const notifierEvent = await this.apiDodleBrokerService.getEvenement(notifier[i].evenement).toPromise();
      setTimeout(() => {
        console.log(notifier[compteur])
        if (notifier[compteur].type == "invite") {
          this.snackBar.open("Vous avez été invité à l'évènement " + notifierEvent.titre, "vu", {
            duration: timeOut
          });
        } else if (notifier[compteur].type == "cloture") {
          this.snackBar.open("L'évènement " + notifierEvent.titre + " à été cloturé pour le " + this.getDate(notifierEvent.dateCloture), "vu", {
            duration: timeOut
          });
        }
        compteur++;
        }, index  * (timeOut+500)); // 500 => temps entre deux messages
        index++;
      }
  }

  deleteEvent(event : Evenement) {
    this.apiDodleBrokerService.deleteEvenement(event).subscribe((event) => {
      this.events.forEach( (item, index) => {
        if(item._id == event._id) this.events.splice(index,1);
      });
    })
  }
  // Formate la date
  getDate(date: Date) {
    var newDate = new Date(date);
    return newDate.getDate() + '/' + newDate.getMonth() + '/' + newDate.getFullYear() + ' ' + newDate.getHours() + ':' + (newDate.getMinutes()<10?'0':'') + newDate.getMinutes();
  }
}
