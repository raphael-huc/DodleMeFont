import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Utilisateur} from '../models/Utilisateur'
import {Evenement} from "../models/Evenement";
import {Creneau} from "../models/Creneau";
import {TypeEvenement} from "../models/TypeEvenement";
import {Invite} from "../models/Invite";
import {Notifier} from "../models/Notifier";
import {Participe} from "../models/Participe";

@Injectable({
  providedIn: 'root'
})
export class ApiDodleBrokerService {

  private url = 'http://localhost:3000/';

  constructor(private httpClient: HttpClient) { }

  //<editor-fold desc="Utilisateur">
  // récupère la liste des Utilisateurs
  public getUtilisateurs(): Observable<Utilisateur[]> {
    return this.httpClient.get<Utilisateur[]>(this.url + 'utilisateur');
  }

  // récupère un utilisateur à partir de son id (ObjectID)
  public getUtilisateur(id : string): Observable<Utilisateur> {
    return this.httpClient.get<Utilisateur>(this.url + 'utilisateur/' + id);
  }

  // récupère un utilisateur à partir de son pseudo
  public getUtilisateurByPseudo(pseudo : string): Observable<Utilisateur> {
    return this.httpClient.get<Utilisateur>(this.url + 'utilisateurByPseudo/' + pseudo);
  }

  // création d'un utilisateur à partir d'un utilisateur
  public createUtilisateur(user: Utilisateur) : Observable<Utilisateur>{
    return this.httpClient.post<Utilisateur>(this.url + 'utilisateur', user);
  }

  // Mise à jour d'un utilisateur à partir d'un utilisateur
  public updateUtilisateur(user: Utilisateur): Observable<Utilisateur>{
    console.log(user);
    return this.httpClient.post<Utilisateur>(this.url + 'utilisateur/' + user._id, user);
  }

  public deleteUtilisateur(user: Utilisateur): Observable<Utilisateur>{
    return this.httpClient.request<Utilisateur>("delete", this.url + 'utilisateur', {body: {id:user._id}});
  }

  //</editor-fold>

  //<editor-fold desc="Evenement">
  // récupère la liste des evenements
  public getEvenements() : Observable<Evenement[]> {
    return this.httpClient.get<Evenement[]>(this.url + 'evenement');
  }

  // récupère un evenement à partir de son id (ObjectID)
  public getEvenement(id : string): Observable<Evenement> {
    return this.httpClient.get<Evenement>(this.url + 'evenement/' + id);
  }

  // récupère les evenements à partir d'un id createur
  public getEvenementByIDCreateur(idCreateur : string): Observable<Evenement[]> {
    return this.httpClient.get<Evenement[]>(this.url + 'evenementByUser/' + idCreateur);
  }

  // récupère les evenements à partir d'un id createur qui sont cloturés
  public getEvenementClotureByIDCreateur(idCreateur : string): Observable<Evenement[]> {
    return this.httpClient.get<Evenement[]>(this.url + 'evenementByUserEventCloture/' + idCreateur);
  }

  // récupère les evenements à partir d'un id createur qui ne sont pas cloturés
  public getEvenementNotClotureByIDCreateur(idCreateur : string): Observable<Evenement[]> {
    return this.httpClient.get<Evenement[]>(this.url + 'evenementByUserEventNotCloture/' + idCreateur);
  }

  // récupère les evenements à partir d'un pseudo createur
  public getEvenementByPseudoCreateur(pseudo : string): Observable<Evenement[]> {
    return this.httpClient.get<Evenement[]>(this.url + 'evenementByPseudo/' + pseudo);
  }

  // récupère les evenements dont est invité une personne à partir de l'id de l'utilisateur
  public getEvenementByInviteIDUser(id : string): Observable<Evenement[]> {
    return this.httpClient.get<Evenement[]>(this.url + 'evenementByInviteUser/' + id);
  }

  public getEvenementByInviteIDUserEventCloture(id : string): Observable<Evenement[]> {
    return this.httpClient.get<Evenement[]>(this.url + 'evenementByInviteUserEventCloture/' + id);
  }

  public getEvenementByInviteIDUserEventNotCloture(id : string): Observable<Evenement[]> {
    return this.httpClient.get<Evenement[]>(this.url + 'evenementByInviteUserEventNotCloture/' + id);
  }


  // récupère le nombre de participation à un évènement en paramètre
  public getNbParticipeByEvent(id : string): Observable<number> {
    return this.httpClient.get<number>(this.url + 'nbParticipeByEvent/' + id);
  }

  // création d'un evenement à partir d'un evenement
  public createEvenement(event: Evenement): Observable<Evenement>{
    return this.httpClient.post<Evenement>(this.url + 'evenement', event);
  }

  public deleteEvenement (event: Evenement): Observable<Evenement> {
    return this.httpClient.request<Evenement>("delete", this.url + 'evenement', {body: {id:event._id}});
  }

  // Mise à jour d'un utilisateur à partir d'un utilisateur
  public updateEvenement(event: Evenement): Observable<Evenement> {
    return this.httpClient.post<Evenement>(this.url + 'evenement/' + event._id, event);
  }

  // Pas testé
  public clotureEvenement(event: Evenement): Observable<Evenement> {
    return this.httpClient.post<Evenement>(this.url + 'evenementCloture/' + event._id, event);
  }
  //</editor-fold>

  //<editor-fold desc="Creneau">
  // récupère la liste des creneaux
  public getCreneaux() : Observable<Creneau[]> {
    return this.httpClient.get<Creneau[]>(this.url + 'creneau');
  }

  // récupère un creneau à partir de son id (ObjectID)
  public getCreneau(id : string): Observable<Creneau> {
    return this.httpClient.get<Creneau>(this.url + 'creneau/' + id);
  }

  // récupère la liste des creneaux
  public getCreneauxByEvenement(idEvent: string): Observable<Creneau[]> {
    return this.httpClient.get<Creneau[]>(this.url + 'creneauByEvent/' + idEvent);
  }

  // récupère la liste des creneaux
  public getCreneauxAndNbParticipeByEvent(idEvent: string): Observable<Object[]> {
    return this.httpClient.get<Object[]>(this.url + 'creneauxAndNbParticipeByEvent/' + idEvent);
  }

  // création d'un creneau à partir d'un creneau
  public createCreneau(creneau: Creneau): Observable<Creneau>{
    return this.httpClient.post<Creneau>(this.url + 'creneau', creneau);
  }

  // Mise à jour d'un Creneau à partir d'un Creneau
  public updateCreneau(creneau: Creneau): Observable<Creneau> {
    return this.httpClient.post<Creneau>(this.url + 'creneau/' + creneau._id, creneau);
  }

  public deleteCreneau(_id: string): Observable<Creneau> {
    return this.httpClient.request<Creneau>('delete', this.url + 'creneau', {body: {id: _id}});
  }
  // </editor-fold>

  // <editor-fold desc="TypeEvenement">
  // récupère la liste des Utilisateurs
  public getTypeEvenements(): Observable<TypeEvenement[]> {
    return this.httpClient.get<TypeEvenement[]>(this.url + 'typeevenement');
  }

  // récupère un utilisateur à partir de son id (ObjectID)
  public getTypeEvenement(id : string): Observable<TypeEvenement> {
    return this.httpClient.get<TypeEvenement>(this.url + 'typeevenement/' + id);
  }

  // création d'un TypeEvenement à partir d'un TypeEvenement
  public createTypeEvenement(typeevenement: TypeEvenement): Observable<TypeEvenement>{
    return this.httpClient.post<TypeEvenement>(this.url + 'typeevenement', typeevenement);
  }

  // Mise à jour d'un TypeEvenement à partir d'un TypeEvenement
  public updateTypeEvenement(typeEvenement: TypeEvenement): Observable<TypeEvenement> {
    return this.httpClient.post<TypeEvenement>(this.url + 'creneau/' + typeEvenement._id, typeEvenement);
  }

  public deleteTypeEvenement (typeEvenement: TypeEvenement): Observable<TypeEvenement> {
    return this.httpClient.request<TypeEvenement>("delete", this.url + 'typeEvenement', {body: {id:typeEvenement._id}});
  }
  //</editor-fold>

  //<editor-fold desc="Invite">
  // récupère la liste des Invite
  public getInvites(): Observable<Invite[]> {
    return this.httpClient.get<Invite[]>(this.url + 'invite');
  }

  // récupère un Invite à partir de son id (ObjectID)
  public getInvite(id : string): Observable<Invite> {
    return this.httpClient.get<Invite>(this.url + 'invite/' + id);
  }

  // récupère un Invite à partir d'un utilisateur
  public getInviteByUtilisateur(id : string): Observable<Invite[]> {
    return this.httpClient.get<Invite[]>(this.url + 'inviteByUser/' + id);
  }

  // récupère un Invite à partir d'un Evenement
  public getInviteByEvenement(id : string): Observable<Invite[]> {
    return this.httpClient.get<Invite[]>(this.url + 'inviteByEvent/' + id);
  }

  public getUtilisateurByEvenement(id : string): Observable<Utilisateur[]> {
    return this.httpClient.get<Utilisateur[]>(this.url + 'invite/userByEvent/' + id);
  }

  // création d'un Invite à partir d'un Invite
  public createInvite(invite: Invite): Observable<Invite>{
    return this.httpClient.post<Invite>(this.url + 'invite', invite);
  }

  public createInviteByPseudos(pseudos: string[], evenement: string): Observable<Invite[]>{
    return this.httpClient.post<Invite[]>(this.url + 'inviteByPseudos',{pseudos: pseudos, evenement: evenement});
  }

  public deleteInvite (invite: Invite): Observable<Invite> {
    return this.httpClient.request<Invite>("delete", this.url + 'invite', {body: {id:invite._id}});
  }
  //</editor-fold>

  //<editor-fold desc="Notifier">
  // récupère la liste des Notifier
  public getNotifiers(): Observable<Notifier[]> {
    return this.httpClient.get<Notifier[]>(this.url + 'notifier');
  }

  // récupère un Notifier à partir de son id (ObjectID)
  public getNotifier(id : string): Observable<Notifier> {
    return this.httpClient.get<Notifier>(this.url + 'notifier/' + id);
  }

  // récupère un Notifier à partir d'un utilisateur
  public getNotifierByUtilisateur(id : string): Observable<Notifier[]> {
    return this.httpClient.get<Notifier[]>(this.url + 'notifierByUser/' + id);
  }

  // récupère un Notifier à partir d'un Evenement
  public getNotifierByEvenement(id : string): Observable<Notifier[]> {
    return this.httpClient.get<Notifier[]>(this.url + 'notifierByEvent/' + id);
  }
  // création d'un Notifier à partir d'un Notifier
  public createNotifier(notifier: Notifier): Observable<Notifier>{
    return this.httpClient.post<Notifier>(this.url + 'notifier', notifier);
  }

  public deleteNotifier (notifier: Notifier): Observable<Notifier> {
    return this.httpClient.request<Notifier>("delete", this.url + 'notifier', {body: {id:notifier._id}});
  }
  //</editor-fold>

  //<editor-fold desc="Participe">
  // récupère la liste des Participe
  public getParticipes(): Observable<Participe[]> {
    return this.httpClient.get<Participe[]>(this.url + 'participe');
  }

  // récupère un Participe à partir de son id (ObjectID)
  public getParticipe(id : string): Observable<Participe> {
    return this.httpClient.get<Participe>(this.url + 'participe/' + id);
  }

  public getParticipesByUtilisateur(id : string): Observable<Participe[]> {
    return this.httpClient.get<Participe[]>(this.url + 'participeByUser/' + id);
  }

  public getParticipesByEvenement(id : string): Observable<Participe[]> {
    return this.httpClient.get<Participe[]>(this.url + 'participeByEvent/' + id);
  }

  public getParticipesByUserCreneau(idUser: string, idCreneau: string): Observable<Participe> {
    return this.httpClient.get<Participe>(this.url + 'participeByUserCreneau/' + idUser + "/" + idCreneau);
  }

  // création d'un Participe à partir d'un Participe
  public createParticipe(participe: Participe): Observable<Participe>{
    return this.httpClient.post<Participe>(this.url + 'participe', participe);
  }

  public getEvenementParticipeUtilisateur(id: String): Observable<Participe[]>{
    return this.httpClient.post<Participe[]>(this.url + 'participe/', id);
  }

  public getUtilisateurParticipeByEvennement(idEvent: String): Observable<Utilisateur[]>{
    return this.httpClient.get<Utilisateur[]>(this.url + 'participe/userByEvent/' + idEvent);
  }

  // Mise à jour d'un Participe à partir d'un Participe
  public updateParticipe(participe: Participe): Observable<Participe> {
    return this.httpClient.post<Participe>(this.url + 'participe/' + participe._id, participe);
  }

  public deleteParticipe (participe: Participe): Observable<Participe> {
    return this.httpClient.request<Participe>("delete", this.url + 'participe', {body: {id:participe._id}});
  }
  //</editor-fold>
}
