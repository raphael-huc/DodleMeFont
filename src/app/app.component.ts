import { Component } from '@angular/core';
import { Utilisateur } from "../models/Utilisateur";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DodleMe';
}
