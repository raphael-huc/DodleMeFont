import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router'; /* Faire les routes */
import {HttpClientModule} from '@angular/common/http';
import { AccueilComponent } from './accueil/accueil.component';

import { ApiDodleBrokerService} from './api-dodle-broker.service';
import { ConnexionComponent } from './connexion/connexion.component';
import { CalendrierComponent } from './calendrier/calendrier.component';
import { ProfilComponent } from './profil/profil.component';
import { ClotureComponent } from './evenement/cloture/cloture.component';
import { TitreComponent } from './evenement/creation/titre/titre.component';
import { DateComponent } from './evenement/creation/date/date.component';
import { InvitationComponent } from './evenement/creation/invitation/invitation.component';
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';
import { AuthService } from "./auth.service";
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

const appRoutes: Routes = [
  // 1 route par module
  { path: 'accueil/:option', component: AccueilComponent},
  { path: '', component: ConnexionComponent},
  { path: 'connexion', component: ConnexionComponent},
  { path: 'calendrier/:id', component: CalendrierComponent},
  { path: 'profil', component: ProfilComponent},
  { path: 'cloture-evenement/:id', component: ClotureComponent},
  { path: 'titre-evenement', component: TitreComponent},
  { path: 'date-evenement/:id', component: DateComponent},
  { path: 'invitation-evenement/:id', component: InvitationComponent},
  { path: 'logout', component: LogoutComponent},
  { path: 'header', component: HeaderComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    ConnexionComponent,
    CalendrierComponent,
    ProfilComponent,
    ClotureComponent,
    TitreComponent,
    DateComponent,
    InvitationComponent,
    LogoutComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, NgbModule, FormsModule, RouterModule.forRoot(appRoutes), HttpClientModule, MaterialModule,
     OwlDateTimeModule, OwlNativeDateTimeModule, ReactiveFormsModule
  ],
  providers: [ApiDodleBrokerService, AuthService, {provide: OWL_DATE_TIME_LOCALE, useValue: 'fr'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
