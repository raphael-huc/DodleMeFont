import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthService {

  constructor(private router: Router) {

    if (JSON.parse(localStorage.getItem('currentUser')) == null) {
      console.log("test");
      this.router.navigate(['/connexion']);
    }

  }
}
