import {Component, Input, OnInit} from '@angular/core';
import {ApiDodleBrokerService} from '../api-dodle-broker.service';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Utilisateur} from '../../models/Utilisateur';
import {Creneau} from "../../models/Creneau";
import {Participe} from "../../models/Participe";
import {Evenement} from "../../models/Evenement";

@Component({
  selector: 'app-calendrier',
  templateUrl: './calendrier.component.html',
  styleUrls: ['./calendrier.component.css']
})
export class CalendrierComponent implements OnInit {

  currentUser: Utilisateur;
  event: Evenement;
  users: Utilisateur[] = new Array<Utilisateur>();
  creneaux: Creneau[];
  participes: Participe[] = new Array<Participe>();
  values: boolean[];
  newValue: boolean;
  participeUser: Participe[];
  dataLoaded: Promise<boolean>;

  constructor(private apiDodleBrokerService: ApiDodleBrokerService,
              private httpClient: HttpClient,
              private router: Router,
              private routeActive: ActivatedRoute) {
  }

  ngOnInit(): void {

    // Récupère l'utilisateur actuel
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    // Recupere l'évènement sur lequel on est
    this.apiDodleBrokerService.getEvenement(this.routeActive.snapshot.paramMap.get("id")).subscribe((event ) => {
      this.event = event;

      // Récupération des creneaux de l'évenement
      this.apiDodleBrokerService.getCreneauxByEvenement(this.event._id).subscribe((data) => {
        this.creneaux = data;
        this.values = new Array<boolean>(this.creneaux.length);
        this.participeUser = new Array<Participe>(this.creneaux.length)
        this.dataLoaded = Promise.resolve(true);
      })

      // Recupération de du créateur de l'évènement, il n'est pas dans la liste des invités car c'est le créateur !
      this.apiDodleBrokerService.getUtilisateur(this.event.createur).subscribe((user) => {
        this.users.push(user);
        // Recupere les utilisateurs qui sont invités à l'évenement
        this.apiDodleBrokerService.getUtilisateurByEvenement(this.event._id).subscribe((users) => {
          console.log(users);
          this.users = this.users.concat(users);
          // Pour chaque utilisateur on récupère si il a déjà donné une réponse pour un creneau
          for(var i = 0; i < this.users.length; i++) {
            this.apiDodleBrokerService.getParticipesByEvenement(this.event._id).subscribe((data) => {
              this.participes = this.participes.concat(data);

              // on check si la personne connecté à déjà donné des réponses pour l'événement
              if(this.participes.find(element => element.utilisateur == this.currentUser._id) === undefined) {
                this.newValue = true;
              } else {
                this.newValue = false;
                // Si oui, alors on les récupères
                this.initializeValue();
              }
            })
          }
        })
      })
    })
  }

  // Formate la date
  getDate(date: Date) {
    var newDate = new Date(date);
    return newDate.getDate() + '/' + newDate.getMonth() + '/' + newDate.getFullYear() + ' ' + newDate.getHours() + ':' + (newDate.getMinutes()<10?'0':'') + newDate.getMinutes();
  }

  // Initialise les valeurs de l'utilisateur courant pour remplir les checkbox
  initializeValue() {
    for(let i = 0; i < this.creneaux.length; i++) {
      let participe = this.participes.find(element => element.utilisateur == this.currentUser._id && element.creneau == this.creneaux[i]._id);
      this.participeUser[i] = participe;
      this.values[i] = participe.reponse;
    }
  }

  // Renvoie un boolean correspondant à la réponse d'un utilisateur pour un creneau
  getReponseParticipe(idUser: string, idCreneau: string) : string{
    let participe : Participe | undefined ;
    participe = this.participes.find(element => element.utilisateur == idUser && element.creneau == idCreneau);
    let bool;
    if(!participe) {
      bool = null;
    } else {
      bool = participe.reponse
    }
    if (bool) {
      return "Possible"
    } else {
      return "Impossible"
    }
  }

  // sauvegarde les participations, deux cas, si c'est des nouvelles valeurs alors on crée une nouvelle participation sinon on l'update
  saveParticipes() {
    if(this.newValue) {
      for(var i = 0; i < this.creneaux.length; i++) {
        let participe = new Participe();
        participe.utilisateur = this.currentUser._id;
        participe.evenement = this.event._id;
        participe.creneau = this.creneaux[i]._id;
        let reponse = this.values[i];
        if (reponse === undefined) {
          reponse = false;
        }
        //console.log(reponse);
        participe.reponse = reponse;

        this.apiDodleBrokerService.createParticipe(participe).subscribe((ptcp) => {
          console.log(ptcp);
          this.router.navigate(['/accueil/all']);
        })
      }
    }  else {
      for(var i = 0; i < this.creneaux.length; i++) {
        this.participeUser[i].reponse = this.values[i];
        this.apiDodleBrokerService.updateParticipe(this.participeUser[i]).subscribe((participe) => {
          console.log(participe);
          this.router.navigate(['/accueil/all']);
        })
      }
    }
  }
}


