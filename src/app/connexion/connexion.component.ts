import {Component, Input, OnInit} from '@angular/core';
import { ApiDodleBrokerService } from '../api-dodle-broker.service';
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {AppComponent} from "../app.component";
import {Utilisateur} from "../../models/Utilisateur";

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  @Input() Username: string;

  constructor(private apiDodleBrokerService: ApiDodleBrokerService,
              private httpClient: HttpClient,
              private router: Router,
              private routeActive: ActivatedRoute) { }

  ngOnInit(): void {
    if( JSON.parse(localStorage.getItem('currentUser')) != null ) {
      this.router.navigate(['/accueil/all']);
    }
  }

  connexion() {
    this.apiDodleBrokerService.getUtilisateurByPseudo(this.Username).subscribe((data) => {
      localStorage.setItem('currentUser', JSON.stringify(data));
      this.router.navigate(['/accueil/all']);
    });
  }
}
