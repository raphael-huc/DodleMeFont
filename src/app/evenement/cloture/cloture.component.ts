import { Component, OnInit } from '@angular/core';
import {Evenement} from '../../../models/Evenement';
import {Utilisateur} from '../../../models/Utilisateur';
import {ApiDodleBrokerService} from '../../api-dodle-broker.service';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Creneau} from "../../../models/Creneau";

@Component({
  selector: 'app-cloture',
  templateUrl: './cloture.component.html',
  styleUrls: ['./cloture.component.css']
})
export class ClotureComponent implements OnInit {

  event: Evenement = new Evenement();
  user: Utilisateur;
  creneaux: Creneau[];
  creneauxSortNbParticipe;

  constructor(public apiDodleBrokerService: ApiDodleBrokerService,
              private httpClient: HttpClient,
              private router: Router,
              private routeActive: ActivatedRoute) {  }

  ngOnInit(): void {
    const option = this.routeActive.snapshot.paramMap.get('id');
    this.apiDodleBrokerService.getEvenement(option).subscribe((event) => {
      this.event = event;
      console.log(event);
    });
    this.apiDodleBrokerService.getCreneauxByEvenement(option).subscribe((creneaux) => {
      this.creneaux = creneaux;
      console.log(creneaux);
    });

    this.apiDodleBrokerService.getCreneauxAndNbParticipeByEvent(option).subscribe((data) => {
      this.creneauxSortNbParticipe = data;
      console.log(this.creneauxSortNbParticipe)
    })
  }

  getDate(date: Date) {
    const newDate = new Date(date);
    return newDate.getDate() + '/' + newDate.getMonth() + '/' + newDate.getFullYear() + ' ' + newDate.getHours() + ':' + newDate.getMinutes();
  }

  updateDate(event: Evenement, date: Date){
    event.dateCloture = date;
    this.apiDodleBrokerService.clotureEvenement(event).subscribe((event) => {
      console.log(event);
    });
    this.router.navigate(['/accueil/all']);
  }
}
