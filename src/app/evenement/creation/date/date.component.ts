import {Component, NgModule, OnInit} from '@angular/core';
import {ApiDodleBrokerService} from '../../../api-dodle-broker.service';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Utilisateur} from '../../../../models/Utilisateur';
import {Creneau} from '../../../../models/Creneau';



@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})


export class DateComponent implements OnInit {

  public dateTime: Date;
  user: Utilisateur;
  creneaux: Creneau[] = new Array();
  dayDate: Date = new Date();

  constructor(public apiDodleBrokerService: ApiDodleBrokerService,
              private httpClient: HttpClient,
              private router: Router,
              private routeActive: ActivatedRoute){ }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  createDate(){
    const tmp = new Creneau();
    if (this.dateTime != null){
      tmp.date = this.dateTime;
      tmp.evenement = this.routeActive.snapshot.paramMap.get('id');
      this.apiDodleBrokerService.createCreneau(tmp).subscribe((data) => {
        console.log(data);
        this.creneaux.push(data);
        console.log(this.creneaux);
      });
    }
    else{
      console.log('Erreur créneaux');
    }
  }

  cancel(id: string, i: number){
    console.log(id);
    this.apiDodleBrokerService.deleteCreneau(id).subscribe((data) => {
      // console.log(creneau);
      this.creneaux.splice(i, 1);
      console.log(this.creneaux);
    });
  }

  goInvitation() {
    this.router.navigate(['/invitation-evenement/' + this.routeActive.snapshot.paramMap.get('id')]);
  }

  getDate(date: Date) {
    const newDate = new Date(date);
    return newDate.getDate() + '/' + newDate.getMonth() + '/' + newDate.getFullYear() + ' ' + newDate.getHours() + ':' + (newDate.getMinutes()<10?'0':'') + newDate.getMinutes();
  }

}
