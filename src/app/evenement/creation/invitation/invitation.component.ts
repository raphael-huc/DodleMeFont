import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {Evenement} from "../../../../models/Evenement";
import {ApiDodleBrokerService} from "../../../api-dodle-broker.service";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {Invite} from "../../../../models/Invite";
import {Utilisateur} from "../../../../models/Utilisateur";

@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.css']
})
export class InvitationComponent implements OnInit {
  name = 'Angular 6';
  form: FormGroup;
  evenement: Evenement;
  user: Utilisateur;

  constructor(private fb: FormBuilder,
              private apiDodleBrokerService: ApiDodleBrokerService,
              private httpClient: HttpClient,
              private router: Router,
              private routeActive: ActivatedRoute) { }


  ngOnInit() {
    const id = this.routeActive.snapshot.paramMap.get("id");
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.form = this.fb.group({
      items: this.fb.array([this.createItem()])
    });
    console.log(id);
    this.apiDodleBrokerService.getEvenement(id).subscribe((event) =>  {
      this.evenement = event;
      console.log(event);
    });
  }

  // crée un item dans le formBuilder
  createItem() {
    return this.fb.group({
      pseudo: ['']
    })
  }

  // Ajoute une ligne dans le formulaire
  addNext() {
    (this.form.controls['items'] as FormArray).push(this.createItem())
  }

  // Sauvegarde les invitations
  submit() {
    console.log(this.form.value["items"]);
    this.apiDodleBrokerService.createInviteByPseudos(this.form.value["items"], this.evenement._id).subscribe((data) => {
      console.log(data);
      this.router.navigate(['/accueil/all']);
    })

  }

  get formData() {
    return <FormArray> this.form.get('items');
  }
}
