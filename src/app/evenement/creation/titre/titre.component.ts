import { Component, OnInit } from '@angular/core';
import {ApiDodleBrokerService} from '../../../api-dodle-broker.service';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Utilisateur} from '../../../../models/Utilisateur';
import {Evenement} from "../../../../models/Evenement";

@Component({
  selector: 'app-titre',
  templateUrl: './titre.component.html',
  styleUrls: ['./titre.component.css']
})
export class TitreComponent implements OnInit {

  user: Utilisateur;
  titre: string;
  description: string;
  selectedCard: string;

  constructor(private apiDodleBrokerService: ApiDodleBrokerService,
              private httpClient: HttpClient,
              private router: Router,
              private routeActive: ActivatedRoute) {}

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  create(){
    const tmp = new Evenement();
    if (this.selectedCard != null){
      tmp.titre = this.titre;
      tmp.description = this.description;
      tmp.typeEvenement = this.selectedCard;
      tmp.createur = this.user._id;
      this.apiDodleBrokerService.createEvenement(tmp).subscribe((data) => {
        this.router.navigate(['/date-evenement/' + data._id]);
      });
    }
    else{
      console.log('Erreur type évènement');
    }
  }

  selectCard(id){
    this.selectedCard = id;
  }

}
