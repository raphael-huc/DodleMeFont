import { Component, OnInit } from '@angular/core';
import {ApiDodleBrokerService} from "../api-dodle-broker.service";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {Utilisateur} from "../../models/Utilisateur";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private apiDodleBrokerService: ApiDodleBrokerService,
              private httpClient: HttpClient,
              private router: Router,
              private routeActive: ActivatedRoute) { }

  ngOnInit(): void {
  }

  isConnected() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  moveAccueil(){
    this.router.navigate(['/accueil/all']);
  }
}
