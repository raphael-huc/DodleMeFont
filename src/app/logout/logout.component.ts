import { Component, OnInit } from '@angular/core';
import {ApiDodleBrokerService} from "../api-dodle-broker.service";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private httpClient: HttpClient,
              private router: Router,
              private routeActive: ActivatedRoute) { }

  ngOnInit(): void {
    localStorage.removeItem("currentUser");
    this.router.navigate(['/connexion']);
  }

}
