import { NgModule } from '@angular/core';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';

const Material = [
  MatSnackBarModule,
  MatDialogModule
  ];

@NgModule({
  imports: [Material],
  exports: [Material]
})
export class MaterialModule {}
