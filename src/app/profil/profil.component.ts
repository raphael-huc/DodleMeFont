import {Component, Input, OnInit} from '@angular/core';
import { ApiDodleBrokerService } from '../api-dodle-broker.service';
import {Utilisateur} from '../../models/Utilisateur';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  user: Utilisateur;
  @Input() mail: string;
  @Input() nom: string;
  @Input() prenom: string;
  @Input() description: string;


  constructor(private apiDodleBrokerService: ApiDodleBrokerService,
              private httpClient: HttpClient,
              private router: Router,
              private routeActive: ActivatedRoute) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.mail = this.user.mail;
    this.prenom = this.user.prenom;
    this.nom = this.user.nom;
    this.description = this.user.description;
  }

  saveUser(){
    if(this.nom !== undefined && this.nom != "") {
      this.user.nom = this.nom;
    }
    if(this.prenom !== undefined && this.nom != "") {
      this.user.prenom = this.prenom;
    }
    if(this.mail !== undefined && this.nom != "") {
      this.user.mail = this.mail;
    }
    if(this.description !== undefined && this.nom != "") {
      this.user.description = this.description;
    }
    console.log(this.user);

    this.apiDodleBrokerService.updateUtilisateur(this.user).subscribe((data) => {
      console.log(data);
      localStorage.setItem('currentUser', JSON.stringify(data));
    });
    this.router.navigate(['/accueil/all']);
  }
}
