export class Evenement {
  _id: string;
  titre: string;
  description: string;
  typeEvenement: string;
  dateCloture: Date;
  createur: string;
}
