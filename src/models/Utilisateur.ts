export class Utilisateur {
  _id: string;
  pseudo: string;
  prenom: string;
  nom: string;
  mail: string;
  description: string;

  public getString(): string {
    return this._id + " : " + this.pseudo + ", prenom : " + this.prenom + ", nom : " + this.prenom;
  }

}
